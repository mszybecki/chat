package szybecki.chat.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import szybecki.chat.Server;
import szybecki.chat.config.security.JwtTokenProvider;
import szybecki.chat.dto.LoginDTO;
import szybecki.chat.dto.RegisterDTO;
import szybecki.chat.repository.UserRepository;
import szybecki.chat.service.UserService;
import szybecki.chat.utility.DatabaseUtility;
import szybecki.chat.utility.TestUtility;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = Server.class)
@ActiveProfiles("test")
@DirtiesContext
@AutoConfigureMockMvc
public class AuthControllerIT {

    private final String DEFAULT_FIRSTNAME = "Kacper";
    private final String DEFAULT_LASTNAME = "Grabowski";
    private final String DEFAULT_LOGIN = "Thouch1948";
    private final String DEFAULT_PASSWORD = "waG9Pah9ai";

    private final static Logger log = LoggerFactory.getLogger(AuthControllerIT.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DatabaseUtility databaseUtility;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private MockMvc mockMvc;

    private RegisterDTO registerDTO;

    private LoginDTO loginDTO;

    @BeforeEach
    public void setup() {
//        final AuthController authController = new AuthController(this.userService, this.authenticationManager, this.jwtTokenProvider);
//        this.mockMvc = MockMvcBuilders.standaloneSetup(authController)
//                .apply(SecurityMockMvcConfigurers.springSecurity())
//                .build();

        this.registerDTO = new RegisterDTO(DEFAULT_FIRSTNAME, DEFAULT_LASTNAME, DEFAULT_LOGIN, DEFAULT_PASSWORD);
        this.loginDTO = new LoginDTO(DEFAULT_LOGIN, DEFAULT_PASSWORD);
    }

    @AfterEach
    public void clearDB() {
        this.databaseUtility.clear();;
    }

    @Test
    @Transactional
    public void testLoginThatLoginIsBad() throws Exception {
        this.userService.create(registerDTO);
        long countBefore = this.userRepository.count();

        this.loginDTO.setLogin("");
        this.mockMvc.perform(post("/api/auth/login")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.loginDTO)))
                .andExpect(status().isUnauthorized());


        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testLoginThatPasswordIsBad() throws Exception {
        this.userService.create(registerDTO);
        long countBefore = this.userRepository.count();

        this.loginDTO.setPassword("");
        this.mockMvc.perform(post("/api/auth/login")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.loginDTO)))
                .andExpect(status().isUnauthorized());


        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testSuccessfulLogin() throws Exception {
        this.userService.create(registerDTO);
        long countBefore = this.userRepository.count();

        this.mockMvc.perform(post("/api/auth/login")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.loginDTO)))
                .andExpect(status().isOk());

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testGetAuthUserThatIsSuccessful() throws Exception {
        this.userService.create(registerDTO);
        long countBefore = this.userRepository.count();

        String token = this.jwtTokenProvider.createToken(DEFAULT_LOGIN);
        this.mockMvc.perform(get("/api/auth/user")
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testGetAuthUserThatFailed() throws Exception {
        this.userService.create(registerDTO);
        long countBefore = this.userRepository.count();

        this.mockMvc.perform(get("/api/auth/user"))
                .andExpect(status().isForbidden());

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testRegisterThatFirstnameIsEmpty() throws Exception {
        long countBefore = this.userRepository.count();

        this.registerDTO.setFirstname("");
        this.mockMvc.perform(post("/api/auth/register")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.loginDTO)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(mvcResult -> {
                    Assertions.assertTrue(mvcResult.getResponse().getContentAsString().matches("^.+firstname.+$"));
                });

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testRegisterThatLastnameIsEmpty() throws Exception {
        long countBefore = this.userRepository.count();

        this.registerDTO.setLastname("");
        this.mockMvc.perform(post("/api/auth/register")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.registerDTO)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(mvcResult -> {
                    Assertions.assertTrue(mvcResult.getResponse().getContentAsString().matches("^.+lastname.+$"));
                });

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testRegisterThatLoginIsEmpty() throws Exception {
        long countBefore = this.userRepository.count();

        this.registerDTO.setLogin("");
        this.mockMvc.perform(post("/api/auth/register")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.registerDTO)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(mvcResult -> {
                    Assertions.assertTrue(mvcResult.getResponse().getContentAsString().matches("^.+login.+$"));
                });

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testRegisterThatLoginExists() throws Exception {
        this.userService.create(registerDTO);
        long countBefore = this.userRepository.count();

        this.mockMvc.perform(post("/api/auth/register")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.registerDTO)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(mvcResult -> {
                    Assertions.assertTrue(mvcResult.getResponse().getContentAsString().matches("^.+login.+$"));
                });

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testRegisterThatPasswordIsEmpty() throws Exception {
        long countBefore = this.userRepository.count();

        this.registerDTO.setPassword("");
        this.mockMvc.perform(post("/api/auth/register")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.registerDTO)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(mvcResult -> {
                    Assertions.assertTrue(mvcResult.getResponse().getContentAsString().matches("^.+password.+$"));
                });

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore, countAfter);
    }

    @Test
    @Transactional
    public void testRegisterThatIsSuccessful() throws Exception {
        long countBefore = this.userRepository.count();

        this.mockMvc.perform(post("/api/auth/register")
                .contentType(TestUtility.APPLICATION_JSON_UTF8)
                .content(TestUtility.convertObjectToJsonBytes(this.registerDTO)))
                .andExpect(status().isCreated());

        long countAfter= this.userRepository.count();
        Assertions.assertEquals(countBefore + 1, countAfter);
    }

}
