package szybecki.chat.utility;

import org.springframework.stereotype.Component;
import szybecki.chat.repository.MessageRepository;
import szybecki.chat.repository.UserRepository;

@Component
public class DatabaseUtility {

    private UserRepository userRepository;

    private MessageRepository messageRepository;

    public DatabaseUtility(UserRepository userRepository, MessageRepository messageRepository) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
    }

    public void clear() {
        this.userRepository.deleteAll();
        this.messageRepository.deleteAll();
    }
}
