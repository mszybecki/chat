package szybecki.chat.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pusher.rest.Pusher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class MiscellaneousConfiguration {

    private final String pusherAppId;

    private final String pusherKey;

    private final String pusherSecret;

    private final String pusherCluster;

    @Autowired
    public MiscellaneousConfiguration(
            @Value("${pusher.id}") String pusherAppId,
            @Value("${pusher.key}") String pusherKey,
            @Value("${pusher.secret}") String pusherSecret,
            @Value("${pusher.cluster}") String pusherCluster) {
        this.pusherAppId = pusherAppId;
        this.pusherKey = pusherKey;
        this.pusherSecret = pusherSecret;
        this.pusherCluster = pusherCluster;
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        return objectMapper;
    }

    @Bean
    public Pusher pusher() {
        Pusher pusher = new Pusher(this.pusherAppId, this.pusherKey, this.pusherSecret);

        pusher.setCluster(this.pusherCluster);
        pusher.setEncrypted(true);

        return pusher;
    }
}
