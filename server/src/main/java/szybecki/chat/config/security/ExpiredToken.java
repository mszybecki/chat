package szybecki.chat.config.security;

public class ExpiredToken extends RuntimeException{
    public ExpiredToken() {
        super("Expired or invalid JWT token");
    }
}
