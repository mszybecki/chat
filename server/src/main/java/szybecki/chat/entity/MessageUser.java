package szybecki.chat.entity;

import lombok.*;

import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@NamedStoredProcedureQuery(name = "MessageUser.last5", procedureName = "last_messages", parameters = {
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "p_user_id", type = Long.class)
})
public class MessageUser {

    private Long message_id;

    private Long user_id;

    private Boolean seen;

    @Override
    public String toString() {
        return "MessageUser{" +
                "message_id=" + message_id +
                ", user_id=" + user_id +
                ", seen=" + seen +
                '}';
    }
}
