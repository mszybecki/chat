package szybecki.chat.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import szybecki.chat.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByLogin(String login);

    @Query("select u from User u where concat(u.firstname, ' ', u.lastname) like concat('%', ?1, '%')")
    List<User> findByName(String name);

    @Query("select u from User u where u.login like ?1")
    Optional<User> findByLoginOptional(String login);
}
