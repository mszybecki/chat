package szybecki.chat.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import szybecki.chat.entity.Message;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

    @Query(value = "call last_messages(:user_id);", nativeQuery = true)
    List<Map<String, Object>> getLastMessages(@Param("user_id") Long userId);

    @Query(value = "call last_messages_next(:user_id, :not_in);", nativeQuery = true)
    List<Map<String, Object>> getLastMessagesNext(@Param("user_id") Long userId, @Param("not_in") String notIn);

    @Query(value = "select m from Message m where (m.receiver.id = ?1 and m.sender.id = ?2) or (m.receiver.id = ?2 and m.sender.id = ?1)")
    List<Message> getByUsers(Long user1, Long user2);

    @Query("select m from Message m where m.id = (select max(tmp.id) from Message tmp where (tmp.receiver.id = ?1 and tmp.sender.id = ?2) or (tmp.receiver.id = ?2 and tmp.sender.id = ?1))")
    Optional<Message> findLastByUsers(Long user1, Long user2);
}
