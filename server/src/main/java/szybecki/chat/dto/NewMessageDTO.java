package szybecki.chat.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NewMessageDTO {
    private Long receiver_id;
    private Long sender_id;
    private String message;
}
