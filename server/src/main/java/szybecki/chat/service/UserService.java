package szybecki.chat.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import szybecki.chat.dto.RegisterDTO;
import szybecki.chat.dto.UserDTO;
import szybecki.chat.entity.User;
import szybecki.chat.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void create(RegisterDTO registerDTO) {
        User user = new User();
        user.setFirstname(registerDTO.getFirstname());
        user.setLastname(registerDTO.getLastname());
        user.setLogin(registerDTO.getLogin());
        user.setPassword(this.passwordEncoder.encode(registerDTO.getPassword()));
        this.userRepository.save(user);
    }

    public Optional<User> find(Long id) {
        return this.userRepository.findById(id);
    }

    public UserDTO findById(Long id) {
        User user = this.userRepository.findById(id).get();
        return new UserDTO(user.getId(), user.getFirstname(), user.getLastname());
    }

    public UserDTO findByLogin(String login) {
        User user = this.userRepository.findByLogin(login);

        if (user == null) {
            return null;
        }

        return new UserDTO(user.getId(), user.getFirstname(), user.getLastname());
    }

    public boolean checkByLogin(String login) {
        User user = this.userRepository.findByLogin(login);

        if (user == null) {
            return false;
        }

        return true;
    }

    public List<UserDTO> search(String name) {
        return this.mapToDto(this.userRepository.findByName(name));
    }

    private UserDTO mapToDTO(User user) {
        return new UserDTO(user.getId(), user.getFirstname(), user.getLastname());
    }

    private List<UserDTO> mapToDto(List<User> users) {
        List<UserDTO> userDTOS = new ArrayList<>();

        for (User user : users) {
            userDTOS.add(this.mapToDTO(user));
        }

        return userDTOS;
    }
}
