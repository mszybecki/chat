package szybecki.chat.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pusher.rest.Pusher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import szybecki.chat.dto.MessageDTO;
import szybecki.chat.dto.NewMessageDTO;
import szybecki.chat.entity.Message;
import szybecki.chat.entity.MessageUser;
import szybecki.chat.entity.User;
import szybecki.chat.repository.MessageRepository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class MessageService {

    private final Pusher pusher;

    private final MessageRepository messageRepository;

    private final ObjectMapper objectMapper;

    private final Logger logger = LoggerFactory.getLogger(MessageService.class);

    public MessageService(Pusher pusher, MessageRepository messageRepository, ObjectMapper objectMapper) {
        this.pusher = pusher;
        this.messageRepository = messageRepository;
        this.objectMapper = objectMapper;
    }

    public void save(NewMessageDTO messageDTO, User sender, User receiver) throws JsonProcessingException {
        Message message = new Message();
        message.setSender(sender);
        message.setReceiver(receiver);
        message.setMessage(messageDTO.getMessage());
        message.setSeen(false);

        message = this.messageRepository.save(message);
        pusher.trigger(
                "user-" + receiver.getId(),
                "message",
                this.objectMapper.writeValueAsString(new MessageDTO(message.getId(), messageDTO.getSender_id(), messageDTO.getReceiver_id(), message.getMessage(), message.getSeen())));
        pusher.trigger(
                "user-" + sender.getId(),
                "message",
                this.objectMapper.writeValueAsString(new MessageDTO(message.getId(), messageDTO.getSender_id(), messageDTO.getReceiver_id(), message.getMessage(), message.getSeen())));
    }

    public MessageDTO findById(Long messageId) {
        Message message = this.messageRepository.findById(messageId).get();
        return new MessageDTO(messageId, message.getSender().getId(), message.getReceiver().getId(), message.getMessage(), message.getSeen());
    }

    public void setSeen(Long messageId) {
        Message message = this.messageRepository.findById(messageId).get();
        message.setSeen(true);
        this.messageRepository.save(message);
    }

    public MessageUser getLastMessage(Long messageId, Long authUser) {
        Message message = this.messageRepository.findById(messageId).get();
        MessageUser messageUser = new MessageUser();
        messageUser.setSeen(message.getSeen());
        messageUser.setMessage_id(messageId);
        messageUser.setUser_id(
                message.getSender().getId().equals(authUser) ?
                        message.getReceiver().getId() :
                        message.getSender().getId());

        return messageUser;
    }

    public List<MessageUser> getLastMessages(Long userId, String notIds) {
        if (notIds.equals("")) {
            return this.map(this.messageRepository.getLastMessages(userId));
        } else {
            return this.map(this.messageRepository.getLastMessagesNext(userId, notIds));
        }
    }

    private List<MessageUser> map(List<Map<String, Object>> dbUsers) {
        List<MessageUser> messageUsers = new ArrayList<>();

        for (Map<String, Object> row : dbUsers) {
            messageUsers.add(new MessageUser(
                    ((BigInteger) row.get("message_id")).longValue(),
                    ((BigInteger) row.get("user_id")).longValue(),
                    (Boolean) row.get("seen")));
        }

        return messageUsers;
    }

    public List<MessageDTO> getByUsers(Long user1, Long user2) {
        return this.mapToDto(this.messageRepository.getByUsers(user1, user2));
    }

    public MessageUser getLastByUsers(Long userId1, Long userAuthId2) {
        Optional<Message> optional = this.messageRepository.findLastByUsers(userId1, userAuthId2);

        if (optional.isEmpty()) {
            return null;
        }

        Message message = optional.get();
        MessageUser messageUser = new MessageUser();
        messageUser.setMessage_id(message.getId());
        messageUser.setSeen(message.getSeen());
        messageUser.setUser_id(message.getReceiver().getId().longValue() == userAuthId2.longValue() ? userId1 : userAuthId2);

        return messageUser;
    }

    private List<MessageDTO> mapToDto(List<Message> messages) {
        List<MessageDTO> result = new ArrayList<>();

        for (Message message : messages) {
            result.add(new MessageDTO(
                    message.getId(),
                    message.getSender().getId(),
                    message.getReceiver().getId(),
                    message.getMessage(),
                    message.getSeen()));
        }

        return result;
    }
}
