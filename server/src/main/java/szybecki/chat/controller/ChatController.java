package szybecki.chat.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import szybecki.chat.dto.MessageDTO;
import szybecki.chat.dto.NewMessageDTO;
import szybecki.chat.dto.UserDTO;
import szybecki.chat.entity.MessageUser;
import szybecki.chat.entity.User;
import szybecki.chat.service.MessageService;
import szybecki.chat.service.UserService;

import java.util.List;
import java.util.Optional;

@Controller
public class ChatController {

    private final UserService userService;

    private final MessageService messageService;

    private static final Logger logger = LoggerFactory.getLogger(ChatController.class);

    public ChatController(UserService userService, MessageService messageService) {
        this.userService = userService;
        this.messageService = messageService;
    }

    @GetMapping("/api/user/search/{name}")
    public ResponseEntity<List<UserDTO>> searchForUsers(@PathVariable String name) {
        return ResponseEntity.ok(this.userService.search(name));
    }

    @GetMapping("/api/user/{user_id}")
    public ResponseEntity<UserDTO> findUserById(@PathVariable("user_id") Long userId) {
        return ResponseEntity.ok(this.userService.findById(userId));
    }

    @GetMapping("/api/message/{message_id}")
    public ResponseEntity<MessageDTO> findMessageById(@PathVariable("message_id") Long messageId) {
        return ResponseEntity.ok(this.messageService.findById(messageId));
    }

    @PostMapping("/api/send-message")
    public ResponseEntity<Void> sendMessage(
            @RequestBody NewMessageDTO messageDTO) {
        logger.info("{}", messageDTO);

        Optional<User> receiver = this.userService.find(messageDTO.getReceiver_id());
        Optional<User> sender = this.userService.find(messageDTO.getSender_id());

        if (receiver.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        try {
            this.messageService.save(messageDTO, sender.get(), receiver.get());
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            logger.error("Error", e);
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/api/last-message/{message_id}")
    public ResponseEntity<MessageUser> getLastMessage(
            @PathVariable("message_id") Long messageId,
            Authentication authentication) {
        return ResponseEntity.ok(
                this.messageService.getLastMessage(
                        messageId,
                        this.userService.findByLogin(authentication.getName()).getId()));
    }

    @GetMapping("/api/last-messages")
    public ResponseEntity<List<MessageUser>> getLastMessage(Authentication authentication) {
        UserDTO userDTO = this.userService.findByLogin(authentication.getName());

        return ResponseEntity.ok(this.messageService.getLastMessages(userDTO.getId(), ""));
    }

    @GetMapping("/api/last-messages-user/{user_id}")
    public ResponseEntity<MessageUser> getLastMessageByUserId(@PathVariable("user_id") Long userId, Authentication authentication) {
        UserDTO user = this.userService.findByLogin(authentication.getName());
        MessageUser messageUser = this.messageService.getLastByUsers(userId, user.getId());

        if (messageUser == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(messageUser);
    }

    @PostMapping("/api/last-messages-more")
    public ResponseEntity<List<MessageUser>> getMoreLastMessage(
            Authentication authentication,
            @RequestBody List<Long> ids) {
        UserDTO userDTO = this.userService.findByLogin(authentication.getName());

        return ResponseEntity.ok(this.messageService.getLastMessages(userDTO.getId(), StringUtils.join(ids, ',')));
    }

    @PatchMapping("/api/message/{message_id}")
    public ResponseEntity<Void> setSeenMessage(@PathVariable("message_id") Long messageId) {
        this.messageService.setSeen(messageId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/api/messages/{user_id}")
    public ResponseEntity<List<MessageDTO>> getByUsers(@PathVariable("user_id") Long userId, Authentication authentication) {
        UserDTO authUser = this.userService.findByLogin(authentication.getName());
        return ResponseEntity.ok(this.messageService.getByUsers(userId, authUser.getId()));
    }

//    public ResponseEntity<Void> getLastMessages() {
//
//    }
}
