package szybecki.chat.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import szybecki.chat.config.security.JwtTokenProvider;
import szybecki.chat.dto.LoginDTO;
import szybecki.chat.dto.RegisterDTO;
import szybecki.chat.dto.UserDTO;
import szybecki.chat.service.UserService;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final UserService userService;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    public AuthController(UserService userService,
              AuthenticationManager authenticationManager,
              JwtTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping("/register")
    public ResponseEntity<Map<String, String>> singup(
            @RequestBody RegisterDTO registerDTO) throws Exception {
        Map<String, String> errors = new HashMap<>();
        boolean error = false;

        if (StringUtils.isEmpty(registerDTO.getFirstname())) {
            error = true;
            errors.put("firstname", "Imię nie może być puste");
        }

        if (StringUtils.isEmpty(registerDTO.getLastname())) {
            error = true;
            errors.put("lastname", "Nazwisko nie może być puste");
        }

        if (StringUtils.isEmpty(registerDTO.getLogin())) {
            error = true;
            errors.put("login", "Login nie może być pusty");
        } else if (registerDTO.getLogin().length() < 5) {
            error = true;
            errors.put("login", "Login musi mieć minimum 5 znaków");
        } else if (this.userService.checkByLogin(registerDTO.getLogin())) {
            error = true;
            errors.put("login", "Taki login już istnieje");
        }

        if (StringUtils.isEmpty(registerDTO.getPassword())) {
            error = true;
            errors.put("password", "Hasło nie może być puste");
        } else if (registerDTO.getPassword().length() < 5) {
            error = true;
            errors.put("password", "Hasło musi mieć minimum 5 znaków");
        }

        if (error) {
            return ResponseEntity.unprocessableEntity().body(errors);
        }

        this.userService.create(registerDTO);
        return ResponseEntity.created(new URI("")).build();
    }

    @PostMapping("/login")
    public ResponseEntity<Map<Object, Object>> signin(
            @RequestBody @Validated LoginDTO loginDTO) throws Exception {
        Map<Object, Object> model = new HashMap<>();

        try {
            String username = loginDTO.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginDTO.getPassword()));
            String token = jwtTokenProvider.createToken(username);


            model.put("token", token);
            return ResponseEntity.ok(model);
        } catch (Exception e) {
            model.put("error", "Złe dane");
            return new ResponseEntity<>(model, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/user")
    public ResponseEntity<UserDTO> getAuthUser(Authentication authentication) {
        logger.info("{}", authentication);
        UserDTO dto = this.userService.findByLogin(authentication.getName());

        if (dto == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(dto);
    }
}
