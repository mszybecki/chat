import Vue from 'vue'
import axios from 'axios'
import Pusher from 'vue-pusher'

import App from './App'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.http = Vue.prototype.$http = axios

Vue.use(Pusher, {
  api_key: '8bc72d14c51efee8d304',
  options: {
    cluster: 'eu',
    encrypted: true
  }
})

new Vue({
  components: { App },
  router,
  store,
  // template: '<App/>',
  render: h => h(App)
}).$mount('#app')
