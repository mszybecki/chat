import { apiUser, apiSearchUsers } from '../../utils/api'

const state = {
  users: [],
  searchedUsers: []
}

const getters = {
  getUser: state => id => state.users.find(user => user.id === id),
  getSearchedUsers: state => state.searchedUsers
}

const actions = {
  fetchUser ({ commit, getters }, id) {
    return new Promise((resolve, reject) => {
      if (!getters.getUser(id)) {
        apiUser(id)
          .then((user) => {
            commit('addUser', user)
            resolve(user)
          }).catch((err) => {
            reject(err)
          })
      }
    })
  },
  searchUsers ({ commit }, userName) {
    return new Promise((resolve, reject) => {
      apiSearchUsers(userName)
        .then(({ data }) => {
          console.log(data)
          data.forEach(user => {
            commit('addUser', user)
          })
          commit('addSearchedUsers', data)
          resolve(data)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  clearSearchedUsers ({ commit }) {
    commit('addSearchedUsers', [])
  }
}

const mutations = {
  addUser (state, user) {
    state.users = state.users.filter((m) => m.id !== user.id)
    state.users = [user, ...state.users]
  },
  addSearchedUsers (state, users) {
    state.searchedUsers = users
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
