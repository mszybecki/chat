import {apiMessage, apiMessages, apiSendMessage, apiUpdateMesage} from '../../utils/api'
import store from '../index'
import { EventBus } from '../../utils/event-bus'

const state = {
  messages: []
}

const getters = {
  getMessage: state => id => state.messages.find(message => message.id === id),
  getMessagesByUserId: state => (userId, authUserId) => state.messages.filter(message => {
    return (message.receiver_id === userId && message.sender_id === authUserId) ||
      (message.sender_id === userId && message.receiver_id === authUserId)
  })
}

const actions = {
  fetchMessage ({ commit, getters }, id) {
    return new Promise((resolve, reject) => {
      if (!getters.getUser(id)) {
        apiMessage(id)
          .then((user) => {
            commit('addMessage', user)
            resolve(user)
          }).catch((err) => {
            reject(err)
          })
      }
    })
  },

  setSeenMessage ({ commit, getters }, id) {
    return new Promise((resolve, reject) => {
      if (!getters.getUser(id)) {
        apiUpdateMesage(id)
          .then((user) => {
            apiMessage(id)
              .then((message) => {
                commit('updateMessage', message)
                resolve()
              }).catch((err) => {
                reject(err)
              })
          }).catch((err) => {
            reject(err)
          })
      }
    })
  },

  fetchMessagesByUserId ({ commit, getters }, id) {
    return new Promise((resolve, reject) => {
      apiMessages(id)
        .then((messages) => {
          commit('setMessages', messages)
          resolve()
        }).catch((err) => {
          reject(err)
        })
    })
  },

  sendMessage ({ commit, getters }, message) {
    return new Promise((resolve, reject) => {
      apiSendMessage(message)
        .then((messages) => {
          resolve()
        }).catch((err) => {
          reject(err)
        })
    })
  },

  fromPusher ({ commit, dispatch }, message) {
    commit('addMessage', message)

    if (store.getters.getCurrentUserOnChat === message.sender_id) {
      dispatch('setSeenMessage', message.id)
    }

    dispatch('fetchLastMessage', message.id)
    EventBus.$emit('needScroll')
  }
}

const mutations = {
  addMessage (state, message) {
    state.messages = [...state.messages, message]
  },
  updateMessage (state, message) {
    state.messages = state.messages.filter((m) => m.id !== message.id)
    state.messages = [...state.messages, message]
  },
  setMessages (state, messages) {
    const ids = messages.map(m => m.id)
    state.messages = state.messages.filter(m => !ids.includes(m.id))
    state.messages = [...state.messages, ...messages]
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
