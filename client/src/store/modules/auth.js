import { apiAuth, apiRegister, apiAuthUser } from '../../utils/api'

const state = {
  token: '',
  user: {
    id: 0,
    firstname: '',
    lastname: ''
  }
}

const getters = {
  isAuthenticated: state => !!state.token,
  getToken: state => state.token,
  getAuthenticatedUser: state => state.user
}

const actions = {
  authLogin ({ commit, dispatch }, user) {
    return new Promise((resolve, reject) => {
      apiAuth(user)
        .then(({ token }) => {
          commit('token', token)
          resolve()
        }).catch(error => {
          reject(error.data)
        })
    })
  },
  authRegister ({ commit, dispatch }, { firstname, lastname, login, password }) {
    return new Promise((resolve, reject) => {
      apiRegister({firstname, lastname, login, password})
        .then((response) => {
          dispatch('authLogin', { login, password })
            .then(() => {
              resolve()
            }).catch((err) => {
              reject(err)
            })
        }).catch(({ data }) => {
          reject(data)
        })
    })
  },
  authLogout ({ commit }) {
    return new Promise(resolve => {
      commit('token', '')
      resolve()
    })
  },
  authUser ({ commit, getters }) {
    return new Promise((resolve, reject) => {
      if (getters.getAuthenticatedUser.id === 0) {
        apiAuthUser()
          .then(({data}) => {
            commit('user', data)
            resolve(data)
          }).catch((error) => {
            reject(error)
          })
      }

      resolve(getters.getAuthenticatedUser.id)
    })
  }
}

const mutations = {
  token (state, token) {
    state.token = token
  },
  user (state, { id, firstname, lastname }) {
    state.user.id = id
    state.user.firstname = firstname
    state.user.lastname = lastname
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
