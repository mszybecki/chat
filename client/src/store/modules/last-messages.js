import { apiLastMessages, apiMoreLastMessages, apiLastMessage, apiLastMessageByUser } from '../../utils/api'

const state = {
  lastMessages: []
}

const getters = {
  getLastMessages: state => state.lastMessages
}

const actions = {
  fetchLastMessages ({ commit, dispatch }) {
    return new Promise((resolve) => {
      apiLastMessages()
        .then((messages) => {
          messages.forEach(m => {
            dispatch('fetchUser', m.user_id)
          })
          commit('lastMessages', messages)
          resolve()
        })
    })
  },

  fetchMoreLastMessages ({ commit, dispatch }, ids) {
    return new Promise((resolve) => {
      apiMoreLastMessages()
        .then((response) => {
          response.forEach(m => {
            dispatch('fetchUser', m.user_id)
            dispatch('fetchMessage', m.message_id)
          })
          commit('lastMessages', response)
          resolve(response)
        })
    })
  },

  fetchLastMessage ({ commit }, messageId) {
    apiLastMessage(messageId)
      .then(({data}) => {
        commit('addLastMessage', data)
      })
  },

  fetchLastMessageByUser ({ commit, dispatch }, userId) {
    apiLastMessageByUser(userId)
      .then(({data}) => {
        dispatch('setSeenMessage', data.message_id)
        commit('addLastMessage', data)
      })
  },

  addFakeLastMessage ({ commit }, userId) {
    commit('addLastMessage', {
      user_id: userId,
      message: '',
      fake: true,
      seen: true,
      message_id: 0
    })
  }
}

const mutations = {
  lastMessages (state, messages) {
    state.lastMessages = [...state.lastMessages, ...messages]
    state.lastMessages = state.lastMessages.sort((a, b) => {
      if (a.id > b.id) return -1
      if (b.id > a.id) return 1
      return 0
    })
  },
  addLastMessage (state, message) {
    state.lastMessages = state.lastMessages.filter((m) => m.user_id !== message.user_id)
    state.lastMessages = [message, ...state.lastMessages]
    state.lastMessages = state.lastMessages.sort((a, b) => {
      if (a.id > b.id) return -1
      if (b.id > a.id) return 1
      return 0
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
