const state = {
  currentUserOnChat: 0
}

const getters = {
  getCurrentUserOnChat: state => state.currentUserOnChat
}

const actions = {
  setCurrentUserOnChat ({commit}, userId) {
    commit('setCurrentUserOnChat', userId)
  }
}

const mutations = {
  setCurrentUserOnChat (state, userId) {
    state.currentUserOnChat = userId
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
