import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import lastMessages from './modules/last-messages'
import user from './modules/user'
import message from './modules/message'
import chat from './modules/chat'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    lastMessages,
    user,
    message,
    chat
  },
  strict: process.env.NODE_ENV !== 'production'
})
