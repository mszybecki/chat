import axios from 'axios'
import store from '../store/index'

const SERVER_API = process.env.VUE_APP_SERVER_API

export const apiAuth = ({ login, password }) => {
  return new Promise((resolve, reject) => {
    axios.post(SERVER_API + '/api/auth/login', {
      login,
      password
    }).then((response) => {
      resolve(response.data)
    }).catch((error) => {
      reject(error.response)
    })
  })
}

export const apiAuthUser = () => {
  const header = 'Bearer ' + store.getters.getToken
  return axios.get(SERVER_API + '/api/auth/user', {
    headers: {
      'Authorization': header
    }
  })
}

export const apiRegister = ({ firstname, lastname, login, password }) => {
  return new Promise((resolve, reject) => {
    axios.post(SERVER_API + '/api/auth/register', {
      firstname,
      lastname,
      login,
      password
    }).then((response) => {
      resolve(response.data)
    }).catch((error) => {
      reject(error.response)
    })
  })
}

export const apiLastMessages = () => {
  return new Promise((resolve, reject) => {
    const header = 'Bearer ' + store.getters.getToken
    axios.get(SERVER_API + '/api/last-messages', {
      headers: {
        'Authorization': header
      }
    })
      .then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error.response)
      })
  })
}

export const apiLastMessage = (messageId) => {
  const header = 'Bearer ' + store.getters.getToken
  return axios.get(SERVER_API + '/api/last-message/' + messageId, {
    headers: {
      'Authorization': header
    }
  })
}

export const apiLastMessageByUser = (userId) => {
  const header = 'Bearer ' + store.getters.getToken
  return axios.get(SERVER_API + '/api/last-messages-user/' + userId, {
    headers: {
      'Authorization': header
    }
  })
}

export const apiMoreLastMessages = () => {
  return new Promise((resolve, reject) => {
    const ids = store.getters.getLastMessages.map(lastMessage => lastMessage.user_id)
    const header = 'Bearer ' + store.getters.getToken

    axios.post(SERVER_API + '/api/last-messages-more', ids, {
      headers: {
        'Authorization': header
      }
    })
      .then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error.response)
      })
  })
}

export const apiUser = (id) => {
  return new Promise((resolve, reject) => {
    const header = 'Bearer ' + store.getters.getToken
    axios.get(SERVER_API + '/api/user/' + id, {
      headers: {
        'Authorization': header
      }
    })
      .then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error.response)
      })
  })
}

export const apiMessage = (id) => {
  return new Promise((resolve, reject) => {
    const header = 'Bearer ' + store.getters.getToken
    axios.get(SERVER_API + '/api/message/' + id, {
      headers: {
        'Authorization': header
      }
    })
      .then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error.response)
      })
  })
}

export const apiSendMessage = (message) => {
  return new Promise((resolve, reject) => {
    const header = 'Bearer ' + store.getters.getToken
    axios.post(SERVER_API + '/api/send-message', message, {
      headers: {
        'Authorization': header
      }
    })
      .then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error.response)
      })
  })
}

export const apiMessages = (id) => {
  return new Promise((resolve, reject) => {
    const header = 'Bearer ' + store.getters.getToken
    axios.get(SERVER_API + '/api/messages/' + id, {
      headers: {
        'Authorization': header
      }
    })
      .then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error.response)
      })
  })
}

export const apiUpdateMesage = (id) => {
  return new Promise((resolve, reject) => {
    const header = 'Bearer ' + store.getters.getToken
    axios.patch(SERVER_API + '/api/message/' + id, {}, {
      headers: {
        'Authorization': header
      }
    })
      .then((response) => {
        resolve()
      }).catch((error) => {
        reject(error)
      })
  })
}

export const apiSearchUsers = (userName) => {
  const header = 'Bearer ' + store.getters.getToken
  return axios.get(SERVER_API + '/api/user/search/' + userName, {
    headers: {
      'Authorization': header
    }
  })
}
